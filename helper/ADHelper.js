import ShareHelper from './ShareHelper'

let bannerConfig = {
  'banner_home': 'adunit-65324c72ceff7dbb',
  'video_daily': 'adunit-9c3e1ddb12267af5',
  'video_daily_game': 'adunit-67551b375bebf27f',
  'banner_game': 'adunit-512081503bcbf886',
  'video_free_tip': 'adunit-f8b2149a25d847e0',
  'video_reward': 'adunit-c265da4b6da9a1ba'
}
export default class ADHelper {

  static createBannerAD(banner_id, show = true) {
    let bannerAd = wx.createBannerAd({
      adUnitId: banner_id,
      style: {
        left: (ViewKit.Device.share().screenWidth - 325) / 2,
        // top: ViewKit.Device.share().screenHeight - 250,
        top: ViewKit.Device.share().screenHeight - 250,
        width: 325
      }
    })

    if (bannerAd == null) {
      return null
    }

    bannerAd.show()
    .then(() => {
      if (bannerAd) {
        bannerAd.style.top = ViewKit.Device.share().screenHeight - bannerAd.style.realHeight
      }
    })

    bannerAd.onError(err => {
      console.log(err)
    })

    return bannerAd
  }

  static createVideoAd(id, success, fail, dataBus) {
    console.log(id);
    let video = wx.createRewardedVideoAd({adUnitId: id})
    video.offLoad()
    video.onLoad(() => {
      console.log('激励视频 广告加载成功')
    })

    video.offError()
    video.onError(err => {
      ShareHelper.share(4, dataBus)
      .then(() => {
        if (success) {
          success()
        }
      }).catch(() => {
          if (fail) {
            fail()
          }
      })

      console.log(err)
    })

    video.offClose()
    video.onClose(res => {
      // 用户点击了【关闭广告】按钮
      // 小于 2.1.0 的基础库版本，res 是一个 undefined
      if (res && res.isEnded || res === undefined) {
        if (success) {
          success()
        }
        // 正常播放结束，可以下发游戏奖励
      } else {
        // 播放中途退出，不下发游戏奖励
        if (fail) {
          fail()
        }
      }
      // video.offClose();
    })
    this.showVideoAd(video)
    return video
  }

  static showVideoAd(rewardedVideoAd) {
    rewardedVideoAd.load()
    .then(() => {
    rewardedVideoAd.show()
    .catch(err => {
      console.log(err);
      rewardedVideoAd.load()
      .then(() => rewardedVideoAd.show())
    })
  })
  .catch(err => console.log(err.errMsg))

  }
}

ADHelper.adConfig = bannerConfig

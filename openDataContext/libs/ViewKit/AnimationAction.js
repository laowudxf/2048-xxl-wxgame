import {valueWithKeyPath, setValueWithKeyPath} from './Helper/Helper'
class AnimationAction {
  constructor(type, value, target) {
    this.type = type
    this.param = value
    // 0 -> ready 1 -> running 2 -> finish
    this.state = 0
    this.target = target
    this.toSec = 0

    //test
    this.test = 0

    this.changedData = {}
  }

  start () {
    switch (this.type) {
      case 'wait':
      return new Promise((s, f) => {
        setTimeout(function () {
          s()
        }, this.param);
      })
      break;
      //todo
      case 'call':
      return new Promise((s, f) => {
        this.param()
        s()
      })
      case 'to':
      this.target.toActions.push(this)
      return new Promise((s, f) => {
        this.startTime = Date.now()
        this.state = 0
        this.finishBlock = s
      })

      default:
      return new Promise()
    }
  }

  //case 'to'
  run(target) {
    let time = Date.now()
    let interval = time - this.startTime

    if (interval >= this.toSec) {
      this.state = 2
      timeScale = 1
      this.applyChangeData(timeScale)
    }

    if (this.state == 2) {
      this.finishBlock()
      target.toActions.shift()
      return
    }

    let timeScale = interval / this.toSec

    //初始化需要改变的数据
    this.collectChangeData()
    this.applyChangeData(timeScale)
  }

  collectChangeData() {
    if (Object.keys(this.changedData).length == 0) {
      for (let key in this.param) {
        let v = this.param[key]
        let currentValue = valueWithKeyPath(this.target, key)
        let offset = v - currentValue
        this.changedData[key] = [currentValue, offset]
      }
    }
  }

  applyChangeData(timeScale) {
    for (let key in this.changedData) {
      let values = this.changedData[key]
      let startValue = values[0]
      let offsetValue = values[1]
      let resultValue = startValue + timeScale * offsetValue
      setValueWithKeyPath(this.target, key, resultValue)
    }
  }

  resetChangeData() {
    this.applyChangeData(-1)
  }
}

export default class Animation {

  constructor(view, option) {
    this.target = view
    this.option = option
    this.actions = []
  }

  //actions
  wait(sec) {
    let action = new AnimationAction('wait', sec, this.target)
    this.actions.push(action)
    return this
  }

  to(param, sec) {
    let action = new AnimationAction('to', param, this.target)
    action.toSec = sec
    this.actions.push(action)
    return this
  }

  call(callBack) {
    let action = new AnimationAction('call', callBack, this.target)
    this.actions.push(action)
    return this
  }

  start() {
    let p = null
    let promises = this.actions.reduce((r, x) => {
      if (r == null) {
        r = x.start()
      } else {
        r = r.then(() => {
          return x.start()
        })
      }
      return r
    }, p)

    promises.then(() => {
      if (this.option.loop == true) {
        this.start()
      }
    })
  }

  static get(view, option = {}) {
    return new Animation(view, option)
  }
}

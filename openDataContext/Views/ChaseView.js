import * as ViewKit from '../libs/ViewKit/index'
import DataBus from '../DataBus'

export default class ChaseView extends ViewKit.View {

  get label1 () {
    if (this._label1 == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 10))
      v.textColor = 'white'
      v.text = '即将超越'
      v.setFont(11, 'bold')
      v.anchorY = 1
      // v.anchorX = 0
      v.textAlign = 'center'
      v.position = new ViewKit.Point(this.avatar.width / 2, this.avatar.y - 5)
      this._label1 = v
    }
    return this._label1
  }

  get label2 () {
    if (this._label2 == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 10))
      v.textColor = 'white'
      v.text = 'name'
      v.anchorY = 0
      v.textAlign = 'center'
      v.setFont(11, 'bold')
      v.position = new ViewKit.Point(this.avatar.width / 2, this.avatar.rect.bottom + 5)
      this._label2 = v
    }
    return this._label2
  }

  get label3 () {
    if (this._label3 == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 10))
      v.textColor = 'white'
      v.text = '0'
      v.anchorY = 0
      v.textAlign = 'center'
      v.setFont(11, 'bold')
      v.position = new ViewKit.Point(this.avatar.width / 2, this.label2.rect.bottom + 5)
      this._label3 = v
    }
    return this._label3
  }

  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 85, 92, 40))
      v.bgColor = '#e5e5e5'
      v.cornerRadiusArr = [0, 20, 20, 0]
      this._bgView = v
    }
    return this._bgView
  }

  get avatar () {
    if (this._avatar == null) {
      let v = null
      v = new ViewKit.ImageView("https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erKGTHNaqbsOPqVjQelVIouRSA7kwHl8A298x0a2pVMNYCXRIZ7ic3EWwkrdusg9nbGQbqbNeJNRYQ/132", ViewKit.Rect.rect(0, 0, 45, 45))
      v.cornerRadius = 5
      v.maskToBounds = true
      v.anchorX = 0
      v.anchorY = 0
      v.position = new ViewKit.Point(0, 85)
      this._avatar = v
    }
    return this._avatar
  }

  setupUI() {
    let views = [this.label1, this.label2, this.label3, this.avatar]
    views.map(x => {
      this.addSubview(x)
    })
    // this.addSubview(this.bgView)
    // this.bgView.addSubview(this.avatar)
    // this.bgView.addSubview(this.label1)
    // this.bgView.addSubview(this.label2)
  }

  get dataModel () {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  update(model) {
    if (model == null) {
      // this.bgView.isHidden = true
      this.isHidden = true
    } else {
      this.isHidden = false
      // this.bgView.isHidden = false
      this.avatar.imageSrc = model.userInfo.avatarUrl
      this.label2.text = model.userInfo.nickname
      this.label3.text = model.userInfo.KVDataList[0].value
      // this.label1.position = new ViewKit.Point(5, this.bgView.height / 2 - 2)
    }
  }
}

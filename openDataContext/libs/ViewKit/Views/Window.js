import View from './View'
import {Rect, Point, Size} from '../util'
import Device from '../Helper/Device'
export default class Window extends View {
  constructor() {
    super(Rect.rect(0, 0, Device.share().screenWidth, Device.share().screenHeight))
    this.rootViewController = null
    this.windowLevel = 0
  }

  get rootViewController () {
    return this._rootViewController
  }

  set rootViewController(value) {
    if (value == null) {
      return
    }
    if (this.rootViewController) {
      this.rootViewController.viewWillDisappear()
      this.rootViewController.view.removeFromParent()
      this.rootViewController.viewDidDisappear()
    }

    this._rootViewController = value
    this.rootViewController.viewWillAppear()
    this.addSubview(this.rootViewController.view)
    this.rootViewController.viewDidAppear()
  }
}

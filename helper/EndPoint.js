import Network from './Network'
const HashSha1 = require('./sha1.js');

class EndPoint {
  //uri
  static serverlogin(data = {}, method = 'GET') {
    return Network.request({
      url: 'wx/onLogin',
      data,
      method
    })
  }

  // action

  static login() {
      return wx.ex_login().then((loginResp) => {
          return this.serverlogin({
            code: loginResp.code,
            name: '',
            icon: ''
          })
      })
  }

  static pass(data = {}, method = 'GET') {
    return Network.request({
      url: 'pass',
      data,
      method
    }, 'stroke')
  }

  static config(data = {}, method = 'GET') {
    return Network.request({
      url: 'config',
      data,
      method
    })
  }

  static share_content(data = {}, method = 'GET') {
    return Network.request({
      url: 'share_content',
      data,
      method
    })
  }

  static modifyUserInfo(type, value, userInfo, method = 'POST') {
    let data = {
        x_token: userInfo.x_token,
        type: type,
        value: '' + value,
        time: Date.now(),
        key: HashSha1(userInfo.id, Date.now() + type + value)
      }
    return Network.request({
      url: 'user/value',
      data,
      method
    })
  }

  static exchangeRedPakage(shop_id, mobile, userInfo, method = 'POST') {
    let data = {
        x_token: userInfo.x_token,
        mobile,
        shop_id,
        name: '匿名',
        address: '匿名',
      }
    return Network.request({
      url: 'shop/exchange',
      data,
      method
    })
  }
}

EndPoint.BaseUrl = ''

export default EndPoint

import * as ViewKit from '../../libs/ViewKit/index'

 class Particle extends ViewKit.ImageView {
  setupUI() {
    this.direct = 0
    this.speed = 0 // point / sec
    this.speed_a = 1000
    this.speed_a_direct = 0
    this.timeInterval = 3000
    this.endAplha = 1
    this.nowTime = Date.now()

    this.speed = 10
    this.direct = Math.PI * 0.8
    this.speed_a_direct = -Math.PI / 2

    ViewKit.Animation.get(this,{loop: true})
    .to({'rotate': Math.PI * 2}, 500)
    .call(() => {
        this.rotate = 0
    })
    .start()
  }

  draw(ctx, rect) {
    this.calcPosition()
    super.draw(ctx, rect)
  }

  calcPosition() {
    let time = Date.now() - this.nowTime
    if (time >= this.timeInterval) {
      this.removeFromParent()
      return
    }

    let t = time / 1000

    let x = this.speed * Math.cos(-this.direct) * t
    let y = this.speed * Math.sin(-this.direct) * t + (this.speed_a * t * t) / 2

    this.translation.x = x
    this.translation.y = y
  }
}

export default class Fireworks extends ViewKit.View {

  setupUI() {
    this.particleNumber = 30
    this.directs = [Math.PI / 180 * 75, Math.PI / 180 * 105]
    this.speeds = [250, 400]
    this.userInteraction = false
    this.createParticle()
    setTimeout(() => {
        this.removeFromParent()
    }, 3000)
  }

  createParticle() {
    for (let i = 0; i < this.particleNumber; i++) {
      let particle =  new Particle(null, ViewKit.Rect.rect(0, 0, 10, 10))
    this.imageSrc = ViewKit.getRes('bgImage')
      particle.bgColor = 'blue'
      let index = i % 4
      particle.imageSrc = ViewKit.getRes('p_' + index)
      particle.direct = Math.random() * (this.directs[1] - this.directs[0]) + this.directs[0]
      particle.speed = Math.random() * (this.speeds[1] - this.speeds[0]) + this.speeds[0]
      particle.cornerRadius = particle.height / 2
      this.addSubview(particle)
    }
  }
}


import * as ViewKit from '../libs/ViewKit/index'
import DataBus from '../DataBus'
import Fireworks from './Particle/Fireworks'

export default class PreAfterView extends ViewKit.View {

  get preUser () {
    if (this._preUser == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 56, 56))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 - 100, this.meView.rect.bottom)
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      this._preUser = v
    }
    return this._preUser
  }

  get meBgView () {
    if (this._meBgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 69, 69))
      v.cornerRadius = v.height / 2
      v.position = this.meView.center
      v.bgColor = '#fbd141'
      this._meBgView = v
    }
    return this._meBgView
  }

  get preBgView () {
    if (this._preBgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 61, 61))
      v.cornerRadius = v.height / 2
      v.position = this.preUser.center
      v.bgColor = 'white'
      this._preBgView = v
    }
    return this._preBgView
  }

  get lastBgView () {
    if (this._lastBgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 61, 61))
      v.cornerRadius = v.height / 2
      v.position = this.lastUser.center
      v.bgColor = 'white'
      this._lastBgView = v
    }
    return this._lastBgView
  }

  get meView () {
    if (this._meView == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 64, 64))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, 180)
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      this._meView = v
    }
    return this._meView
  }

  get meLabel () {
    if (this._meLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 60, 24))
      v.textAlign = 'center'
      v.textColor = '#cd5700'
      v.bgColor = '#ffc80d'
      v.text = ''
      v.anchorY = 0
      v.setFont(14)
      v.cornerRadius = 3
      v.position = new ViewKit.Point(this.width / 2, this.meView.rect.bottom - 5)
      this._meLabel = v
    }
    return this._meLabel
  }

  get me_calc_bg () {
    if (this._me_calc_bg == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('calc_me_bg', 'openDataContext/images/'),ViewKit.Rect.rect(0, 0, 100, 24))
      v.position = new ViewKit.Point(this.meView.center.x, this.meView.rect.bottom)
      this._me_calc_bg = v
    }
    return this._me_calc_bg
  }

  get preLabel () {
    if (this._preLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 60, 20))
      v.textAlign = 'center'
      v.textColor = 'white'
      v.text = '等待挑战'
      v.anchorY = 0
      v.setFont(14)
      v.position = new ViewKit.Point(this.preUser.center.x, this.preUser.rect.bottom + 10)
      this._preLabel = v
    }
    return this._preLabel
  }

  get lastLabel () {
    if (this._lastLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 60, 20))
      v.textAlign = 'center'
      v.textColor = 'white'
      v.text = ''
      v.anchorY = 0
      v.setFont(14)
      v.position = new ViewKit.Point(this.lastUser.center.x, this.lastUser.rect.bottom + 10)
      this._lastLabel = v
    }
    return this._lastLabel
  }

  get lastUser () {
    if (this._lastUser == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 56, 56))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 + 100, this.meView.rect.bottom)
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      this._lastUser = v
    }
    return this._lastUser
  }

  setupUI() {
    let views = [this.me_calc_bg, this.meBgView, this.preBgView, this.lastBgView, this.meView, this.preUser, this.lastUser, this.meLabel, this.preLabel, this.lastLabel]
    views.map(x => {
        this.addSubview(x)
    })
  }


  setupData() {
    DataBus.share().getBehandAfterInfo(DataBus.share().openId, DataBus.share().currendScore)
    .then(data => {
        let self = data[1]
        this.meView.imageSrc = self.avatarUrl
        let selfValue = self.KVDataList[0].value
        this.meLabel.text = selfValue + '分'

        let pre = data[0]
        let after = data[2]
        if (pre) {
            this.preUser.imageSrc = pre.avatarUrl
            let value = pre.KVDataList[0].value + '分'
            this.preLabel.isHidden = false
            this.preLabel.text =  value
          this.preUser.isHidden = false
          this.preBgView.isHidden = false
        } else {
          this.preLabel.isHidden = true
          this.preUser.isHidden = true
          this.preBgView.isHidden = true
        }

        if (after) {
            this.lastUser.imageSrc = after.avatarUrl
            let value = after.KVDataList[0].value + '分'
            this.lastLabel.text =  value
            this.lastUser.isHidden = false
            this.lastLabel.isHidden = false
            this.lastBgView.isHidden = false
        } else {
            this.lastUser.isHidden = true
            this.lastLabel.isHidden = true
            this.lastBgView.isHidden = true
        }
    })

  }

  refresh() {
    this.setupData()
    // this.setupFirework()
  }

  setupFirework() {
    let v = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
    v.position = new ViewKit.Point(this.width / 2 - 50, 150)
    this.addSubview(v)

    setTimeout(() => {
      let v1 = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
      v1.position = new ViewKit.Point(this.width / 2, 150)
      this.addSubview(v1)

    }, 100)

    setTimeout(() => {
      let v2 = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
      v2.position = new ViewKit.Point(this.width / 2 + 50, 150)
      this.addSubview(v2)

    }, 200)

  }
}

let dataBusInstance

export default class DataBus {
  constructor() {
    if (dataBusInstance) {
      return dataBusInstance
    }

    this.isBoardShow = false
    dataBusInstance = this
  }

  static share() {
      return new DataBus()
  }

  saveHighestScore(highestScore) {
    wx.setUserCloudStorage({
      KVDataList: [{key: 'highestScore', value: String(highestScore)}],
      success: res => {
        console.log(res);
      },
      fail: res => {
        console.log(res);
      }
    })
  }

   getFirendScore(shareTicket) {
     return new Promise((s, f) => {
       let param =  {
         keyList: ['highestScore'],
         success: (data) => {
           let d = data.data

           let e = []
           d.map(x => {
             if (x.KVDataList.length > 0) {
               e.push(x)
             }
           })

           let result = e.sort((a, b) => {
             if (a.KVDataList.length == 0) {
               return true
             }

             if (b.KVDataList.length == 0) {
               return false
             }

             let av = parseInt(a.KVDataList[0].value)
             let bv = parseInt(b.KVDataList[0].value)

             let f = bv - av

             return f
           })

           result.map((x, i) => x.sort = i)
           s(result)
         },
         fail: err => {
           console.log(err);
           f(err)
         }
       }

       if (shareTicket) {
         param['shareTicket'] = shareTicket
         wx.getGroupCloudStorage(param)
       } else {
         wx.getFriendCloudStorage(param)
       }
     })
   }

   getChaseUserInfo(selfScore) {
     return new Promise((s, f) => {
       let param =  {
         keyList: ['highestScore'],
         success: (data) => {
           let d = data.data
           let e = []
           d.map(x => {
             if (x.KVDataList.length > 0) {
               e.push(x)
             }
           })
           console.log(e, 111111111111);
           let result = e.sort((a, b) => {
             if (a.KVDataList.length == 0) {
               return true
             }

             if (b.KVDataList.length == 0) {
               return false
             }

             let av = parseInt(a.KVDataList[0].value)
             let bv = parseInt(b.KVDataList[0].value)

             let f = bv - av
             return f
           })

           result.map((x, i) => x.sort = i)

           let chaseResult = null
           // if (selfScore == 0) {
           //   return s(chaseResult)
           // }

           for (let i = 0; i < result.length; i++) {
             let v = parseInt(result[i].KVDataList[0].value)
             if (v < selfScore) {
               let index = i - 1
               if (index < 0) {
                 return s(null)
               }
               chaseResult = {
                 offset : 0,
                 userInfo: result[index]
               }
               return s(chaseResult)
             }
           }

           chaseResult = {
             offset : 0,
             userInfo: result[result.length - 1]
           }
           return s(chaseResult)
         },
         fail: err => {
           console.log(err);
           f(err)
         }
       }

       wx.getFriendCloudStorage(param)
     })
   }

   getBehandAfterInfo(selfOpenId, selfScore) {
     return new Promise((s, f) => {
       let param =  {
         keyList: ['highestScore'],
         success: (data) => {
           let d = data.data
           let e = []
           d.map(x => {
             if (x.KVDataList.length > 0) {
               e.push(x)
             }
           })

           let selfInfo = e.filter((x) => x.openid == selfOpenId)
          selfInfo[0].KVDataList[0].value = selfScore + ''

           let result = e.sort((a, b) => {
             let av = parseInt(a.KVDataList[0].value)
             let bv = parseInt(b.KVDataList[0].value)

             let f = bv - av
             return f
           })

           result.map((x, i) => x.sort = i)
           console.log(result, 5555555555);


          let sort = selfInfo[0].sort
          let preSort = sort - 1
          let afterSort = sort + 1

          let preInfo = null
          let afterInfo = null

          if (preSort >= 0) {
            preInfo = result[preSort]
          }

          if (afterSort < result.length) {
            afterInfo = result[afterSort]
          }
          s([preInfo, selfInfo[0], afterInfo])


           // if (selfScore == 0) {
           //   return s(chaseResult)
           // }
           //
           // for (let i = 0; i < result.length; i++) {
           //   let offset = selfScore - parseInt(result[i].KVDataList[0].value)
           //   if (offset == 0 && (i - 1) >= 0) {
           //     offset = result[i - 1].KVDataList[0].value - selfScore
           //     chaseResult = {
           //       offset,
           //       // userInfo: result[i - 1]
           //       userInfo: result[i - 1]
           //     }
           //     s(chaseResult)
           //     return
           //   }
           // }
           // s(chaseResult)
         },
         fail: err => {
           console.log(err);
           f(err)
         }
       }

       wx.getFriendCloudStorage(param)
     })
   }

 }

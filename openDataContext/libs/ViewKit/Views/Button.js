import View from './View'
import Label from './Label'
import {Rect, Point, Size, drawUsingArc, screenScale} from '../util'
import EventListenerController from '../EventListenerController'
import ImageView from './ImageView'
import Animation from '../AnimationAction'
import * as Gesture from '../GestureRecognzer/GestureRecognzer'
import SoundHelper from '../Helper/SoundHelper'

export default class Button extends View {

  inlineSetup() {
    this.name = 'Button'
    this.setupLabel()
    this.text = 'button'

    this.userInteraction = true
    this.bgColor = null
    this.icon = null
    this.clickedBlock = null
    this.soundName = Button.soundName
    let ges = new Gesture.TapGestureRecognzer(this, (e) => {
      if (this.soundName) {
        SoundHelper.playSoundWithName(this.soundName, () => {
          if (this.clickedBlock) {
            this.clickedBlock()
          }
        })
        return
      }
      if (this.clickedBlock) {
        this.clickedBlock()
      }
    })
    this.addGesture(ges)

    // this.tapStateBlock = (value) => {
    //   if (value) {
    //     Animation.get(this)
    //     .to({'scale.x': 1.1, 'scale.y': 1.1}, 100)
    //     .start()
    //   } else {
    //     Animation.get(this)
    //     .to({'scale.x': 1, 'scale.y': 1}, 100)
    //     .start()
    //   }
    // }
  }

  set text(value) {
    this._text =  value
    if (this.label) {
      this.label.text = value
    }
  }

  get text() {
    return this._text
  }

  setupLabel() {
    // this.label = new Label(Rect.rect(0, 0, 10, 20))
    this.label = new Label()
    this.label.text = this.text
    this.label.setFit()
    this.label.textColor = 'blue'
    this.label.position = this.rect.center
    this.addSubview(this.label)
  }

  get bgImageView () {
    if (this._bgImageView == null) {
      this._bgImageView = new ImageView('', Rect.rect(0, 0, this.rect.width, this.rect.height))
    }
    return this._bgImageView
  }


  get imageView () {
    if (this._imageView == null) {
      this._imageView = new ImageView(this.icon, Rect.rect(0, 0, 20, 20))
    }
    return this._imageView
  }

  set bgImage(value) {
    this._bgImage = value
    this.bgImageView.imageSrc = value
  }

  get bgImage() {
    return this._bgImage
  }

  /*
  touchEvent(e) {
    // if ((e.type == 'touchend' || e.type == 'ontouchend') && this.clickedBlock != null) {
    //   this.clickedBlock()
    // }
    super.touchEvent(e)
  }
  */

  draw(ctx, rect) {
    if (this.bgImage != null && this.bgImage != '') {
      if (this.bgImageView.parentView == null) {
        this.insertSubView(this.bgImageView, 0)
      }
    }
    super.draw(ctx, rect)
  }

  // --------- experiment
  setFit() {
    this.shouldBeFit = true
    this.bgImageView.setFit()
    this.bgImageView.updateSize = (v) => {
      this.width = v.width
      this.height = v.height
    }
  }

}

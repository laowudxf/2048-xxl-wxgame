/**
 * 请在白鹭引擎的Main.ts中调用 platform.login() 方法调用至此处。
 */

import './helper/WxObjectExtension'
const HashSha1 = require('./sha1.js');
import Storage from './helper/Storage'
import ADHelper from './helper/ADHelper'
import ShareHelper from './helper/ShareHelper'
import EndPoint from './helper/EndPoint'
class WxgamePlatform {

    name = 'wxgame'
    userInfo = {}
    Storage = Storage
    dataBus = null

    sha1(uid, s) {
      return HashSha1(uid, s)
    }

    createVideoAd(id, success, fail, dataBus) {
      return ADHelper.createVideoAd(id, success, fail, dataBus)
    }

    constructor() {
      this.wx = wx
      this.sharedCanvas = sharedCanvas
      this.EndPoint = EndPoint
        // wx.aldSendEvent("音效")
      this.fs = wx.getFileSystemManager()
      this.isDebug = false
      this.baseUrl = this.isDebug ? 'https://caige-app.sl.idler8.com/program/' : 'https://caige1.sanliwenhua.com/program/'
      this.baseKey = this.isDebug ? 68 : 68

      this.userInfo = null

      this.getSystemInfo()
      .then((info) => {
        if (info.model.indexOf('iPhone') != -1) {
          this.is_iPhone = true
        } else {
          this.is_iPhone = false
        }
      })
    }

    postMessage(data) {
      const openDataContext = wx.getOpenDataContext();
      openDataContext.postMessage(data);
    }

    setupSharedCanvase() {
      sharedCanvas.width = sharedCanvas.width * Device.share().scale
      sharedCanvas.height = sharedCanvas.height * Device.share().scale
      this.postMessage({method: 'shouldReDraw'})
    }

    addShare() {
      if (!this.userInfo) {
        return
      }

      wx['request']({
        method: 'GET',
        url: this.baseUrl  + 'shars',
        data: {
          x_token: this.userInfo['x_token'],
          is_group: 0,
          app: this.baseKey
        },
        success: (data) => {
          console.log('share')
          console.log(data)
          if (data.data.code != '200') {
            console.log('user/value fail')
            console.log
            return
          }
        }
      })
    }


      onShow(block) {
        wx.onShow((data) => {
          console.log(data);
          block(data)
        })
      }

      showModel(data) {
        wx.showModal(data)
      }

      navigateToMiniProgram(appId) {
        wx.navigateToMiniProgram({
          appId: appId
        })
      }

      getSystemInfo() {
        return new Promise((s, f) => {
          s(wx.getSystemInfoSync())
        })
      }

      login() {
        return new Promise((resolve, reject) => {
          wx.login({
            success: (res) => {
              resolve(res)
            }
          })
        })
      }

      getUserInfo() {
        return new Promise((resolve, reject) => {
          resolve({})
        })
    }

    setupScope() {
      return new Promise((s, f) => {
        wx.authorize({
          scope: 'scope.userInfo',
          success: (res) => {
            s(res)
          },
          fail: (res) => {
            f(res)
          }
        })
      })
    }

    openDataContext = new WxgameOpenDataContext();

    getUserInfoButton() {
      return new Promise((s, f) => {
        let button = wx.createUserInfoButton({
          type: 'text',
          text: '获取用户信息',
          style: {
            left: 10,
            top: 76,
            width: 200,
            height: 40,
            lineHeight: 40,
            backgroundColor: '#ff0000',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: 16,
            borderRadius: 4
          }
        })

        button.onTap((res) => {
          console.log(res)
          s(res)
        })

      })
    }

    getStorage(key) {
      return new Promise((s, f) => {
        this.fs.readFile({filePath:`${wx.env.USER_DATA_PATH}/${key}`, 'encoding': 'utf8',
        success: (data) => {
          console.log('readfile key:' + key);
          console.log(data);
          try {
            let res = JSON.parse(data.data);
            s({ data: res })
          } catch (e) {
            s({ data: data.data })
          }
        },
        fail: (err) => {
          console.log('read file fail key:' + key);
          console.log(err)
          s({data: null})
        }
      })

    })
  }

  setStorage(arg) {

    return new Promise((s, f) => {
      let key = arg.key
      if (key == null || arg.data == null) {
        s(null)
        return
      }

      let data = ""

      if ( arg.data instanceof Object) {
        data = JSON.stringify(arg.data)
      } else {
        data = arg.data
      }

      console.log(arg.data instanceof Object);
      console.log('saving data');
      console.log(data);
      this.fs.writeFile(
        {
          filePath: `${wx.env.USER_DATA_PATH}/${key}`,
          data:data,
          encoding: 'utf8',
          success: (data) => {
            s()
          },
          fail: (err) => {
            console.log('err key:' + key)
            console.log(err);
            f()
          }
        }
      )
    })
        // return window.localStorage.setItem(arg.key, typeof arg.data == 'object' ? JSON.stringify(arg.data) : arg.data);
  }

  getUserToken(code, userInfo) {
    return EndPoint.login()
    .then(data => {
      console.log(data);
      this.userInfo = data.data.data
      this.postMessage({
        method: 'setOpenId',
        param: {openId: this.userInfo.openid}
      })
      return data.data.data
    })
  }

  FriendScoreInit() {
    return new Promise((s, f) => {
      this.openDataContext.postMessage({
        command: 'GetFriend',
        openid: this.userInfo.openid,
        key: 'highestScore'
      })
      s()
    })
  }

  getUserSort() {
    return new Promise((s, f) => {
      this.openDataContext.postMessage({
        command: 'SortFriend',
      })
      s()
    })
  }

  InitScore(arg) {
    return new Promise((s, f) => {
      this.openDataContext.postMessage({
        command: 'Init',
        arg
      })
      s()
    })
  }

  SetScore(score) {
    return new Promise((s, f) => {
      this.openDataContext.postMessage({
        command: 'SetScore',
        arg: score
      })
      s()
    })
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  getRandomShareData() {

    let list = [
      ['点不点就在一念之间，真实纠结死人了！', '01'],
      ['进来吧！疯狂的大门向你敞开着！', '02'],
      ['没白活，让我赶上一款好游戏！', '03'],
      ['女人的兴趣，男人永远不会知道！', '04'],
      ['没有体验，就没有快感！', '05'],
      ['消除2048，消除烦恼，消除一切..', '06'],
      ['消除六角，消除烦恼，消除红尘...', '07'],
    ]

    let index = this.getRandomInt(list.length)
    let info = list[index]

    // let d = {
    //   title: info[0],
    //   imageUrl: 'resource/share_' + info[1] + '.jpg'
    // }

    let d = {
      title: '消除2048，消除烦恼，消除一切..',
      imageUrl: 'resource/share_' + '08' + '.jpg'
    }
    return d
  }

  shareAppMessage(callback) {
    this.addShare()
    wx.showShareMenu({
      withShareTicket: true
    })
    return ShareHelper.share(4, this.dataBus)
    // let d = this.getRandomShareData()
    //
    // wx.shareAppMessage(d)
  }

  onShareAppMessage(callback) {
    wx.showShareMenu({
      withShareTicket: true
    });


    wx.onShareAppMessage(function(){
      return ShareHelper.shareInfo(callback)
    })
  }

  showBoard() {

    return new Promise((s, f) => {
      this.openDataContext.postMessage({
        command: 'Board',
        arg: score
      })
      s()
    })
  }
}

class WxgameOpenDataContext {

  createDisplayObject(type, width, height) {
    const bitmapdata = new egret.BitmapData(sharedCanvas);
    bitmapdata.$deleteSource = false;
    const texture = new egret.Texture();
    texture._setBitmapData(bitmapdata);
    const bitmap = new egret.Bitmap(texture);
    bitmap.width = sharedCanvas.width = width;
    bitmap.height = sharedCanvas.height = height;



    if (egret.Capabilities.renderMode == "webgl") {
      const renderContext = egret.wxgame.WebGLRenderContext.getInstance();
      const context = renderContext.context;
      ////需要用到最新的微信版本
      ////调用其接口WebGLRenderingContext.wxBindCanvasTexture(number texture, Canvas canvas)
      ////如果没有该接口，会进行如下处理，保证画面渲染正确，但会占用内存。
      if (!context.wxBindCanvasTexture) {

        // egret.stopTick(null)
        // let flag = 0
        egret.startTick((timeStarmp) => {
          // flag++

          // if (flag < 60 && type == 1) {
          //   return true
          // }
          // flag = 0

          egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
          bitmapdata.webGLTexture = null;
          return false;
        }, this);
      }
    }
    return bitmap;
  }

  postMessage(data) {
    const openDataContext = wx.getOpenDataContext();
    openDataContext.postMessage(data);
  }
}

window.platform = new WxgamePlatform();

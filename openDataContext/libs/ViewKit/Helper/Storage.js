
export default class Storage {
  constructor() {

  }

  static set(key, value) {
    if (this.fs == null) {
      return
    }
    let v = {
      key,
      value
    }
    this.fs.writeFileSync(`${wx.env.USER_DATA_PATH}/` + key, JSON.stringify(v), 'utf8')
  }

  static path(key) {
      return `${wx.env.USER_DATA_PATH}/` + key
  }

  static get(key) {
    if (this.fs == null) {
      return null
    }
    let resp = this.fs.readFileSync(`${wx.env.USER_DATA_PATH}/` + key, 'utf8')
    if (resp == null) {
      return null
    }
    let r = JSON.parse(resp)
    if (r) {
      return r.value
    }
    return null
  }

  unSet(key) {
    wx.unlinkSync(`${wx.env.USER_DATA_PATH}/` + key)
  }
}

if (wx.getFileSystemManager) {
  Storage.fs = wx.getFileSystemManager()
}

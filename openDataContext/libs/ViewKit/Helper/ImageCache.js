import Storage from './Storage'
import WXApi  from './WXApi'
export default class ImageCache {
  constructor() {
    if (ImageCache.shareInstance) {
      return ImageCache.shareInstance
    }
    ImageCache.shareInstance = this
  }

  static share(){
    return new ImageCache()
  }

  // setup() {
  //   this.memCacheMap = {}
  // }
  //
  // getCache(url) {
  //   let result = null
  //   result = this.memChache(url)
  //   return result
  // }
  //
  // memCache(url) {
  //   let result = null
  //   result = this.memCacheMap[url]
  //   if (result == null) {
  //       result = this.diskCache(url)
  //       this.memCacheMap[url] = result
  //   }
  //   return result
  // }
  //
  // diskCache(url) {
  //
  // }

  getCache(url) {
    if (WXApi.FileManager.access == null) {
      return Promise.resolve({url, cached: true})
    }
    return url.indexOf('http') == 0
    ? Promise.resolve(`${wx.env.USER_DATA_PATH}/${encodeURIComponent(url)}`).then(filePath => {
      return WXApi.FileManager.stat({ path: filePath })
      .then(() => {
        return {url: filePath, cached: true}
      })
      .catch(() => {
        return Promise.resolve({url, cached: false})
        // return WXApi.downloadFile({ url }).then(res => {
        //    return WXApi.FileManager.saveFile({ tempFilePath: res.tempFilePath, filePath })
        // });
      })
  })
  : Promise.resolve({url, cached: true});
  }
}

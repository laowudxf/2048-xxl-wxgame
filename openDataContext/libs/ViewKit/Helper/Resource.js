
function getRes(path, baseUrl = 'images/', ext = 'png') {
  return baseUrl + path + '.' + ext
}

export { getRes }

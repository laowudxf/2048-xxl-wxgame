export default class ShareHelper {

  static share(validTime = 4, dataBus) {

    wx.aldSendEvent("分享")

    let share_content = dataBus.share_content.concat()
    console.log(share_content);
    if (dataBus.preProduct_get == false) {
      share_content.shift()
    }
    if (share_content.length == 0) {
      return
    }
    let r_index = this.getRandomInt(share_content.length)
    if (dataBus.preProduct_get) {
        r_index = 0
    }
    let share_d = share_content[r_index]


    if (validTime) {
      dataBus.shareTime = Date.now()
    }

    let p = new Promise((s, f) => {
      if (validTime) {
        dataBus.onShowBlocks.push(() => {
          let offset = (Date.now() - dataBus.shareTime)
          console.log('shareTime:', dataBus.shareTime);
          console.log('offset:', offset);
          console.log('validTime:', validTime);
          if (offset > (validTime * 1000)) {
            s()
          } else {
            f()
          }
        })
      } else {
        s()
      }
    })

    wx.shareAppMessage({
      title: share_d.title,
      imageUrl: share_d.pic
    })

    return p
  }

  static shareInfo(dataBus) {

    let share_content = dataBus.share_content.concat()
    if (dataBus.preProduct_get == false) {
      share_content.shift()
    }

    if (share_content.length == 0) {
      return
    }

    let r_index = this.getRandomInt(share_content.length)
    if (dataBus.preProduct_get) {
        r_index = 0
    }
    let share_d = share_content[r_index]


    console.log('-----------222222222');
    return {
      title: share_d.title,
      imageUrl: share_d.pic
    }
  }

  static getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
}

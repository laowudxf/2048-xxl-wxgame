
import ViewController from './ViewController'

export default class NavigationController extends ViewController {
  constructor(rootViewController) {
    super()
    this.addChild(rootViewController)
  }

  // viewDidLoad() {
  //   this.push(this.rootViewController)
  // }

  push(vc) {
    this.addChild(vc)
  }

  pop() {
    if (this.currentViewController) {
      this.currentViewController.removeFromParent()
    }
  }

  popToRootViewController() {
    for (let i = (this.viewControllers.length - 1); i > 0; i--) {
      this.viewControllers[i].removeFromParent()
    }
  }

}

function valueWithKeyPath (object ,keyPath) {
  let keys = keyPath.split('.')
  let value = object
  for (let k in keys) {
    value = value[keys[k]]
  }
  return value
}

function setValueWithKeyPath (object ,keyPath, nextValue) {
  let keys = keyPath.split('.')
  let value = object

  for (let i = 0; i < (keys.length - 1); i++) {
    value = value[keys[i]]
  }
  value[keys[keys.length - 1]] = nextValue
}


export { valueWithKeyPath, setValueWithKeyPath }
